from world import World

def show_world(world, population=None):
    """ Clears the screen, displays the given world and waits for the user to press enter."""

    if not "generation" in show_world.__dict__.keys():
        show_world.generation = 0
        if show_world.generation == 0:
            show_world.initial_population = population
            show_world.peak_population = population
            show_world.bottom_population = population

    # clear screen
    print("\x1b[2J\x1b[1;1H", end="")

    if population:
        print("Population:", population, " ", end="")
        
        show_world.peak_population = max(population, show_world.peak_population)
        show_world.bottom_population = min(population, show_world.bottom_population)

        if show_world.initial_population > 0:
            print(int(100 * population / show_world.initial_population), "% of initial population. (peak:",
                  show_world.peak_population, " bottom:", show_world.bottom_population, ")")

    print("Generation:", show_world.generation)
    print()

    boundaries = world.get_boundaries()
    if not boundaries:
        print("All cells dead!!")
        return

    (ymin,ymax),(start,end) = boundaries

    if start > 2:
        start -= 2
    end += 2

    for y in range(ymin, ymax+1):
        for x in range(start, end):
            if world.is_alive(x,y):
                print("\x1b[1;31m*", end="")
            else:
                print(" ", end="")
        print()
    print("\x1b[0m")
    
    show_world.generation += 1
#    print("Press enter to continue")
#    input()


def load_world(pattern = ["  ***", " *  *", "*   *"]):
    """ Returns a populated world, if no pattern given a default population. """

    max_w = 0
    for l in pattern:
        if len(l) > max_w:
            max_w = len(l)
    rows = len(pattern) * 50
    columns = max_w * 50

    print(columns, rows)
    world = World(columns, rows, False)

    start_y = int(columns/2 - len(pattern) / 2)
    start_x = int(rows/2 - max_w/2)

    for y in range(len(pattern)):
        for x in range(len(pattern[y])):
            if not pattern[y][x] == ' ':
                world.alive(start_x + x, start_y + y)

    return world
